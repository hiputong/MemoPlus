package com.example.memoplus;

import android.app.Application;
import android.util.Log;

/**
 * @author putong zbx996@gmail.com
 * @date 2023/12/11 20:18
 * @description 用于进行应用级别的初始化和提供全局的上下文。
 */
public class App extends Application {

    // 日志tag
    private static final String TAG = "App";

    private static App instance;

    public static App getInstance() {
        return instance;
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Log.d(TAG, "Application onCreate executed");
    }



}
