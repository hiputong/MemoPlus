package com.example.memoplus.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * @author putong zbx996@gmail.com
 * @date 2023/12/12 16:41
 * @description 用于处理数据库操作的类。
 */
public class MemoDbHelper extends SQLiteOpenHelper {

    // 均为静态，因为只用初始化一次就够了
    private static final String TAG = "MemoDbHelper";
    private static final String DATABASE_NAME = "memo_database";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "memo";
    // 主键，唯一标识每一条备忘录
    public static final String COLUMN_ID = "id";
    // 备忘录标题
    public static final String COLUMN_TITLE = "title";
    // 备忘录内容
    public static final String COLUMN_CONTENT = "content";
    // 创建时间
    public static final String COLUMN_CREATION_TIME = "creation_time";
    // 最后修改时间
    public static final String COLUMN_LAST_MODIFIED_TIME = "last_modified_time";
    // 提醒时间
    public static final String COLUMN_REMINDER_TIME = "reminder_time";
    // 是否已完成
    public static final String COLUMN_IS_COMPLETED = "is_completed";
    // 附件信息（例如文件路径或链接）
    public static final String COLUMN_ATTACHMENTS = "attachments";
    // 优先级
    public static final String COLUMN_PRIORITY = "priority";
    // 标签
    public static final String COLUMN_TAGS = "tags";
    // 位置信息
    public static final String COLUMN_LOCATION = "location";
    // 重复规则
    public static final String COLUMN_REPEAT_RULE = "repeat_rule";


    public MemoDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: 创建数据库");
        // 创建备忘录表
        String createTableQuery = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TITLE + " TEXT, " +
                COLUMN_CONTENT + " TEXT, " +
                COLUMN_CREATION_TIME + " TEXT, " +
                COLUMN_LAST_MODIFIED_TIME + " TEXT, " +
                COLUMN_REMINDER_TIME + " TEXT, " +
                COLUMN_IS_COMPLETED + " INTEGER, " +
                COLUMN_ATTACHMENTS + " TEXT, " +
                COLUMN_PRIORITY + " INTEGER, " +
                COLUMN_TAGS + " TEXT, " +
                COLUMN_LOCATION + " TEXT, " +
                COLUMN_REPEAT_RULE + " TEXT" +
                ")";

        db.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
