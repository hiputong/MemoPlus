package com.example.memoplus.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.memoplus.App;

import java.util.ArrayList;
import java.util.List;

/**
 * @author putong zbx996@gmail.com
 * @date 2023/12/12 16:41
 * @description 用于封装数据访问的仓库类。
 */
public class MemoRepository {

    private static final String TAG = "MemoRepository";
    private static final MemoDbHelper memoDbHelper = new MemoDbHelper(App.getInstance());
    private static final SQLiteDatabase db = memoDbHelper.getReadableDatabase();

    // 查询全部，并按照id倒序
    @SuppressLint("Range")
    public static List<Memo> getAllMemos() {
        return getMemos(null, null, null, null, "id desc");
    }


    // 显示已完成的 Memo
    @SuppressLint("Range")
    public static List<Memo> getFinshedMemos() {
        return getMemos(MemoDbHelper.COLUMN_IS_COMPLETED+"=?", new String[]{"1"}, null, null, "id desc");
    }


    // 显示未完成的 Memo
    @SuppressLint("Range")
    public static List<Memo> getUnFinshedMemos() {
        return getMemos(MemoDbHelper.COLUMN_IS_COMPLETED+"=?", new String[]{"0"}, null, null, "id desc");
    }



    // 新增备忘录
    public static void insertMemo(Memo memo) {
        // 判空，无果没数据则不执行
        if (memo == null) {
            Log.d(TAG, "insertMemo: 空备忘录！");
            return;
        }
        ContentValues contentValues = new ContentValues();
        // contentValues.put(MemoDbHelper.COLUMN_ID, memo.getId()); // id 为主键，自动增长，故可以注释
        contentValues.put(MemoDbHelper.COLUMN_TITLE, memo.getTitle());
        contentValues.put(MemoDbHelper.COLUMN_CONTENT, memo.getContent());
        contentValues.put(MemoDbHelper.COLUMN_CREATION_TIME, memo.getCreationTime());
        contentValues.put(MemoDbHelper.COLUMN_LAST_MODIFIED_TIME, memo.getLastModifiedTime());
        contentValues.put(MemoDbHelper.COLUMN_REMINDER_TIME, memo.getReminderTime());
        contentValues.put(MemoDbHelper.COLUMN_IS_COMPLETED, memo.isCompleted());
        contentValues.put(MemoDbHelper.COLUMN_ATTACHMENTS, memo.getAttachments());
        contentValues.put(MemoDbHelper.COLUMN_PRIORITY, memo.getPriority());
        contentValues.put(MemoDbHelper.COLUMN_TAGS, memo.getTags());
        contentValues.put(MemoDbHelper.COLUMN_LOCATION, memo.getLocation());
        contentValues.put(MemoDbHelper.COLUMN_REPEAT_RULE, memo.getRepeatRule());
        // 执行插入
        db.insert(MemoDbHelper.TABLE_NAME, null, contentValues);
    }

    // 根据id删除备忘录
    public static void delMemoById(long id) {
        // 判空，如果 id 为空则不执行
        if (id < 0) {
            Log.d(TAG, "delMemoById: id < 0");
            return;
        }
        // 执行删除
        db.delete(MemoDbHelper.TABLE_NAME, "id = ?", new String[]{String.valueOf(id)});
        Log.d(TAG, "delMemoById: 已删除id为 " + id + "的备忘录！");
    }


    // 根据id修改备忘录
    public static void updateMemoById(Memo memo) {
        // 判空，判断id是否合法
        if (memo.getId() < 0) {
            Log.d(TAG, "updateMemoById: 无效修改");
            return;
        }
        // 封装需要更新的数据
        ContentValues contentValues = new ContentValues();
        contentValues.put(MemoDbHelper.COLUMN_ID, memo.getId());
        contentValues.put(MemoDbHelper.COLUMN_TITLE, memo.getTitle());
        contentValues.put(MemoDbHelper.COLUMN_CONTENT, memo.getContent());
        contentValues.put(MemoDbHelper.COLUMN_CREATION_TIME, memo.getCreationTime());
        contentValues.put(MemoDbHelper.COLUMN_LAST_MODIFIED_TIME, memo.getLastModifiedTime());
        contentValues.put(MemoDbHelper.COLUMN_REMINDER_TIME, memo.getReminderTime());
        contentValues.put(MemoDbHelper.COLUMN_IS_COMPLETED, memo.isCompleted());
        contentValues.put(MemoDbHelper.COLUMN_ATTACHMENTS, memo.getAttachments());
        contentValues.put(MemoDbHelper.COLUMN_PRIORITY, memo.getPriority());
        contentValues.put(MemoDbHelper.COLUMN_TAGS, memo.getTags());
        contentValues.put(MemoDbHelper.COLUMN_LOCATION, memo.getLocation());
        contentValues.put(MemoDbHelper.COLUMN_REPEAT_RULE, memo.getRepeatRule());
        // 执行更新
        db.update(MemoDbHelper.TABLE_NAME, contentValues, "id=?", new String[]{String.valueOf(memo.getId())});
        Log.d(TAG, "updateMemoById: 已成功更新id为" + memo + "的备忘录");
    }



    // 查询
    @SuppressLint("Range")
    public static List<Memo> getMemos(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        ArrayList<Memo> memoArrayList = new ArrayList<>();
        // 执行查询
        Cursor cursor = db.query(MemoDbHelper.TABLE_NAME, null, selection, selectionArgs, groupBy, having, orderBy);
        Log.d(TAG, "getMemos: 查询全部完毕，共查询到" + cursor.getCount() + "条数据！");
        while (cursor.moveToNext()) {
            // 取 id
            long id = cursor.getLong(cursor.getColumnIndex(MemoDbHelper.COLUMN_ID));
            // 取 content
            String content = cursor.getString(cursor.getColumnIndex(MemoDbHelper.COLUMN_CONTENT));
            // 取时间
            String creationTime = cursor.getString(cursor.getColumnIndex(MemoDbHelper.COLUMN_CREATION_TIME));
            boolean isCompleted = cursor.getInt(cursor.getColumnIndex(MemoDbHelper.COLUMN_IS_COMPLETED)) == 1 ? true : false;
            Memo memo = new Memo(id, content, creationTime, isCompleted);
            memoArrayList.add(memo);
        }
        return memoArrayList;
    }
}
