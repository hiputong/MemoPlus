package com.example.memoplus.data;


import java.io.Serializable;

/**
 * @author putong zbx996@gmail.com
 * @date 2023/12/11 21:34
 * @description 备忘录实体，这里设置了很多冗余参数，以便于后期扩展使用
 */

// 这里实现了 Serializable 接口，为了方便在视图间传递数据
public class Memo implements Serializable {
    private long id; // 唯一标识每一条备忘录
    private String title; // 备忘录标题
    private String content; // 备忘录内容
    private String creationTime; // 创建时间
    private String lastModifiedTime; // 最后修改时间
    private String reminderTime; // 提醒时间
    private boolean isCompleted; // 是否已完成
    private String attachments; // 附件信息（例如文件路径或链接）
    private int priority; // 优先级
    private String tags; // 标签
    private String location; // 位置信息
    private String repeatRule; // 重复规则（重复提醒）

    // 无参构造方法
    public Memo() {
    }

    // 简易的构造方法
    public Memo(String content, String creationTime) {
        this.content = content;
        this.creationTime = creationTime;
    }

    public Memo(long id, String content, String creationTime) {
        this.id = id;
        this.content = content;
        this.creationTime = creationTime;
    }

    public Memo(long id, String content, String creationTime, boolean isCompleted) {
        this.id = id;
        this.content = content;
        this.creationTime = creationTime;
        this.isCompleted = isCompleted;
    }

    public Memo(long id, String title, String content, String creationTime, String lastModifiedTime, String reminderTime, boolean isCompleted, String attachments, int priority, String tags, String location, String repeatRule) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creationTime = creationTime;
        this.lastModifiedTime = lastModifiedTime;
        this.reminderTime = reminderTime;
        this.isCompleted = isCompleted;
        this.attachments = attachments;
        this.priority = priority;
        this.tags = tags;
        this.location = location;
        this.repeatRule = repeatRule;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public String getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(String reminderTime) {
        this.reminderTime = reminderTime;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRepeatRule() {
        return repeatRule;
    }

    public void setRepeatRule(String repeatRule) {
        this.repeatRule = repeatRule;
    }

    @Override
    public String toString() {
        return "Memo{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", creationTime=" + creationTime +
                ", lastModifiedTime=" + lastModifiedTime +
                '}';
    }
}
