package com.example.memoplus.util;

/**
 * @author putong zbx996@gmail.com
 * @date 2023/12/11 21:44
 * @description 常用的日期时间处理工具方法
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm";

    // 格式化日期
    public static String formatDate(Date date) {
        if (date == null) {
            return DEFAULT_DATE_FORMAT;
        }
        return formatDate(date, DEFAULT_DATE_FORMAT);
    }

    // 格式化日期，自定义格式
    public static String formatDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(date);
    }

    // 解析日期字符串
    public static Date parseDate(String dateString) {
        return parseDate(dateString, DEFAULT_DATE_FORMAT);
    }

    // 解析日期字符串，自定义格式
    public static Date parseDate(String dateString, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        try {
            return sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 计算日期差
    public static long daysBetweenDates(Date startDate, Date endDate) {
        long startTime = startDate.getTime();
        long endTime = endDate.getTime();
        return (endTime - startTime) / (1000 * 60 * 60 * 24);
    }
}

